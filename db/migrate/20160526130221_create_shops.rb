class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string :name
      t.string :title
      t.text :description
      t.string :key
      t.references :image

      t.timestamps null: false
    end
  end
end
