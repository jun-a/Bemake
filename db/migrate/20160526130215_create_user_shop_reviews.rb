class CreateUserShopReviews < ActiveRecord::Migration
  def change
    create_table :user_shop_reviews do |t|
      t.references :user
      t.references :shop
      t.integer :score
      t.text :content

      t.timestamps null: false
    end
  end
end
