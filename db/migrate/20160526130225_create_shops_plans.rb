class CreateShopsPlans < ActiveRecord::Migration
  def change
    create_table :shops_plans do |t|
      t.references :shop
      t.string :title
      t.text :description
      t.integer :price


      t.timestamps null: false
    end
  end
end
