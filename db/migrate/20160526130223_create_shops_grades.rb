class CreateShopsGrades < ActiveRecord::Migration
  def change
    create_table :shops_grades do |t|
      t.references :grade


      t.timestamps null: false
    end
  end
end
