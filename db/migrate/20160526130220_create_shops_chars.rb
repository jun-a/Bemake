class CreateShopsChars < ActiveRecord::Migration
  def change
    create_table :shops_chars do |t|
      t.references :shop
      t.references :characteristic
      t.integer :status

      t.timestamps null: false
    end
  end
end
