class CreateClientAdminUsers < ActiveRecord::Migration
  def change
    create_table :client_admin_users do |t|
      t.references :user
      t.references :client


      t.timestamps null: false
    end
  end
end
