class CreateShopsImages < ActiveRecord::Migration
  def change
    create_table :shops_images do |t|
      t.references :shop
      t.references :image


      t.timestamps null: false
    end
  end
end
