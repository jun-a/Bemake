class CreateShopsUsers < ActiveRecord::Migration
  def change
    create_table :shops_users do |t|
      t.references :shop
      t.references :user


      t.timestamps null: false
    end
  end
end
