class CreateGrades < ActiveRecord::Migration
  def change
    create_table :grades do |t|
      t.string :name
      t.string :description
      t.references :image

      t.timestamps null: false
    end
  end
end
