class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :password
      t.string :encrypt_password
      t.datatime :confirmed_at
      t.string :key

      t.timestamps null: false
    end
  end
end
