class CreateShopInfos < ActiveRecord::Migration
  def change
    create_table :shop_infos do |t|
      t.references :shop
      t.references :area
      t.string :shop_name
      t.string :address
      t.integer :tel_number
      t.string :shop_url



      t.timestamps null: false
    end
  end
end
